      ******************************************************************
      * Author:
      * Date:
      * Purpose:
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. THRU.

       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
       01 WSV-NAME PIC X(7).
       01 WSV-LASTNAME PIC X(6).
       01 WSV-AGE PIC 9(2).

       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
           PERFORM REQUEST-DATA.
       REQUEST-AGE.
           MOVE 28 TO WSV-AGE.
       REQUEST-NAME.
           MOVE "ELLANDE" TO WSV-NAME.
       REQUEST-LASTNAME.
           MOVE "MEDINA" TO WSV-LASTNAME.
       REQUEST-DATA.
           PERFORM REQUEST-NAME THRU REQUEST-LASTNAME.
           PERFORM REQUEST-AGE.
           DISPLAY "I AM " WSV-NAME " " WSV-LASTNAME
           " AND I HAVE " WSV-AGE " YEARS OLD".
           STOP RUN.
       END PROGRAM THRU.
