      ******************************************************************
      * Author:
      * Date:
      * Purpose:
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. PERFORM-TIMES.

       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
       01 WSV-COUNT PIC 9(3).

       PROCEDURE DIVISION.

       MAIN-PROCEDURE.
           PERFORM ADD-1 100 TIMES.
           STOP RUN.

       ADD-1.
           ADD 1 TO WSV-COUNT.
           DISPLAY WSV-COUNT.

       END PROGRAM PERFORM-TIMES.
