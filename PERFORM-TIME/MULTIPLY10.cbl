      ******************************************************************
      * Author:ELLANDE MEDINA
      * Date:11/07/22
      * Purpose:
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. MULTIPLY-10.

       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
       01 WSV-Y-OR-N PIC X.
       01 WSV-MULTIPLIER PIC 9(2).
       01 WSV-MULTIPLYING PIC 9(10).
       01 WSV-RESULT PIC 9(11).

       PROCEDURE DIVISION.
       MAIN-PROCEDURER.

       INIT.
           DISPLAY "DO YO WANT MULTIPLY 10 TIMES? Y/N".
           ACCEPT WSV-Y-OR-N
           IF WSV-Y-OR-N = "Y" OR "y"
               PERFORM ACCEPT-MULTIPLYING.
           IF WSV-Y-OR-N = "N" OR "n"
               PERFORM END-PROGRAM
           ELSE
               PERFORM INIT.

       END-PROGRAM.
           STOP RUN.

       ACCEPT-MULTIPLYING.
           DISPLAY "ENTER A NUMBER TO MULTIPLY".
           ACCEPT WSV-MULTIPLYING.
           IF WSV-MULTIPLYING > 0
               PERFORM MULTIPLIER 10 TIMES
               PERFORM RESET-MULTIPLIER
           ELSE
               PERFORM ACCEPT-MULTIPLYING.

       MULTIPLIER.
           ADD 1 TO WSV-MULTIPLIER.
           MULTIPLY WSV-MULTIPLYING BY WSV-MULTIPLIER GIVING
           WSV-RESULT.
           DISPLAY WSV-MULTIPLYING " * " WSV-MULTIPLIER " = "
           WSV-RESULT.

       RESET-MULTIPLIER.
           MOVE 0 TO WSV-MULTIPLIER.

       END PROGRAM MULTIPLY-10.
