      ******************************************************************
      * Author:
      * Date:
      * Purpose:
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. UNTIL.

       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
       01 COUNTER PIC 9(3).

       PROCEDURE DIVISION.

       INIT.
           PERFORM OPERATION UNTIL COUNTER = 100.
           STOP RUN.
       OPERATION.
           ADD 1 TO COUNTER.
           DISPLAY COUNTER.


       END PROGRAM UNTIL.
